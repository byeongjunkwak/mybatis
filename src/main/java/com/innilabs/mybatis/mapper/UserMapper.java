package com.innilabs.mybatis.mapper;

import java.util.List;

import org.apache.catalina.Contained;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.innilabs.mybatis.dto.Page;
import com.innilabs.mybatis.entity.Comment;
import com.innilabs.mybatis.entity.Post;
import com.innilabs.mybatis.entity.User;

@Mapper
public interface UserMapper {

    @Select({
        "SELECT *",
        "FROM member",
        "WHERE uid = #{uid}" // #uid는 집어넣는값 uid는 테이블에 있는값 
    })
    User MemberLogin(@Param("uid") String strLogInId); // 메소드 그대로 가져와서 넣어줌 uid  를 strLogInId와 비교 >> 구현의 역순 

    @Select({
        "SELECT *",
        "FROM member",
        "WHERE uid = #{uid}" 
        //html문서에 입력된 uid라는 값이랑 같다는 조건 하에 모든 테이블 반환 
    })
    User findByUserByUid(@Param("uid") String uid); //어디에 변수로 들어온 uid냐는 말

    @Insert({
        "INSERT INTO member (uid, name, password, email, create_at)",
        "VALUES (#{uid}, #{name}, #{password}, #{email}, #{createAt})" //각각테이블에 받은 각각 값들 넣어줌 
    })
    int insertUser(User user); //왜 여기 반환타입이 int임? 

    //User에 있는값 하나하나를 user이라고 치는거고 그 user에 하나씩 다른 값들이 차례로 들어옴에따라 insertUser메소드에 그 값들이 파라메터로 들어감 
    //여기서는 위에 Insert SQL문들에 나와있는 그대로 각 테이블에 차례로 값들이 저장되어짐 

    @Insert({
        "INSERT INTO bbs (uid, title, body, create_at, update_dt)",
        "VALUES (#{uid}, #{title}, #{body}, #{createAt}, #{createAt})"
    })
    int insertpost(Post post);


    @Select({
        "SELECT *", 
        "FROM bbs a, member b",
        "WHERE a.uid = b.uid",
        "ORDER BY update_dt desc",
        "OFFSET #{pagecnt}*#{pagesize} limit #{pagesize}" 
    })
    List<Post> findAll(Page criteria);


    @Select({
        "SELECT count(*) ", 
        "FROM bbs", 
    })
    int countall();

    // @Select({
    //     "SELECT title", 
    //     "FROM bbs",
    //     "WHERE title = #{searchKeyword}",
    //     "ORDER BY update_dt desc",
    //     "OFFSET #{pagecnt}*#{pagesize} limit #{pagesize}" 
    // })List<Post> findByTitleContaining(Criteria criteria, @Param("searchKeyword") String searchKeyword);

    @Select({
        "SELECT *",
        "FROM bbs",
        "WHERE bid = #{bid}",
    })
    Post findPostByBid(@Param("bid") int bid); //이렇게 bid가 일치하면 다 뽑아온다


    @Delete({
        "DELETE FROM bbs *",
        "WHERE bid = #{bid}" 
    })
    void deleteByBid (int bid);



    @Delete({
        "DELETE FROM annotation *",  //
        "WHERE aid = #{aid}",
    })
    void deleteByAId(int aid);



    @Update({
        "UPDATE bbs", 
        "SET title = #{title}, body= #{body}, update_dt= #{updateDt}",
        "WHERE bid = #{bid}"
    })
    void updateByID(Post post);

    //넣어주기 게시물의 bid가 일치해야함 

    //넣기
    @Insert({
        "INSERT INTO annotation (bid, uid, contents, create_at)",
        "VALUES (#{bid}, #{uid}, #{contents}, #{createAt})"
    })
    int insertcomment(Comment comment);

    //가져오기 
    @Select({
        "SELECT *",
        "FROM annotation",
        "WHERE bid = #{bid}", //bid가 같은 항목들을 다가져와줘
    })
    List<Comment> findCommentByBid(@Param("bid") int bid); //파라메터로 넣어줄게 비교해서

    
}
