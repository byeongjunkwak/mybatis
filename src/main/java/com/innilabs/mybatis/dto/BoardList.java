package com.innilabs.mybatis.dto;

import java.util.List;

import com.innilabs.mybatis.entity.Post;

import lombok.Data;

@Data
public class BoardList {
    int total;
    List<Post> list;
}
