package com.innilabs.mybatis.dto;

import lombok.Data;

@Data
public class LoginReq {
    String username;
    String password;
}
