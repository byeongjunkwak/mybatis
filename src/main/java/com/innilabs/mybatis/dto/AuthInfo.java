package com.innilabs.mybatis.dto;

import lombok.Data;

@Data
public class AuthInfo {
    public String uid;
    public String name;
}
