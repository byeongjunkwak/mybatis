package com.innilabs.mybatis.dto;

import lombok.Data;

@Data
public class ListRes {
    int nowPage;
    int startPage;
    int endPage;
    BoardList list;
}
