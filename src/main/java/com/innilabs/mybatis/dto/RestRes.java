package com.innilabs.mybatis.dto;

import lombok.Data;

@Data
public class RestRes<T> {
    int code;  // 1000: success, 2000: auth, 
    String msg;

    T contents;
    
    public RestRes() {
        this.code = 1000;
        this.msg = "success";
    }

    public RestRes(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
