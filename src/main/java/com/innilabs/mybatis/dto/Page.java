package com.innilabs.mybatis.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

public class Page {

  private Integer pagecnt; //페이지번호

  private Integer pagesize; //페이지당 출력갯수

  public Page(){
    this(0,10);
  }

  public Page(Integer pagecnt, Integer pagesize){
    this.pagecnt = pagecnt;
    this.pagesize = pagesize;
  }
}
