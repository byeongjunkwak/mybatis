package com.innilabs.mybatis.service;

import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;

import com.innilabs.mybatis.dto.BoardList;
import com.innilabs.mybatis.dto.Page;
import com.innilabs.mybatis.entity.Comment;
import com.innilabs.mybatis.entity.Post;
import com.innilabs.mybatis.entity.User;
import com.innilabs.mybatis.mapper.UserMapper;
import com.innilabs.mybatis.utils.StringUtil;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserService {
    final UserMapper userMapper;

    public User getUserInfo(String uid) {
        return userMapper.findByUserByUid(uid);
    }

    String checkUser(User user) { //checkUser라는 메소드 생성 user을 파라메터로 넣어줌

        if (StringUtil.isBlank(user.getName())) return "이름을 넣어주세요";

        //각각 항목들이 입력되지않았을때 띄워주는 메세지창들 각각 만들어줌 쓴 항목이 없으면 isblank함수가 true를 반환하니까 리턴값인 이름을 넣어주세요 출력 유저이름이 들어왔으면 false로 빠져나감

        if (StringUtil.isBlank(user.getEmail())) return "메일주소를 넣어주세요";
        if (StringUtil.isBlank(user.getPassword())) return "비밀번호를 넣어주세요";
        if (StringUtil.isBlank(user.getUid())) return "아이디를 넣어주세요";

        return null; //들어와서 조건문 결과가 false뜨면 null return 좋은거임 ㅇㅇ 썻다는거니까
    }

    public String registUser(User user) {
        //registUser라는 메소드 생성 user을 역시 파라메터로 넣어줌
        String ret = checkUser(user);
        //ret이 checkUser의 함수에 user라는값이 파라메터로 들어왔을때의 결과값
        if (ret == null) {
            //위 함수에서 null값 리턴됐으면 사용자가 값입력한거니까 그렇게되면
            user.setCreateAt(new Date()); //입력된 시간 알려주는 메서드실행
            userMapper.insertUser(user); // 사용자의 입력값들을 하나씩 DB에 저장하는 메서드실행
        }
        return ret; //아니면 null값리턴 어차피 여긴 의미없으니까 돌아갈때도 없고 ㅇㅇ.
    }

    public User MemberLogin(String strLogInId, String strPassword){
        User user = userMapper.MemberLogin(strLogInId);


        if(strPassword != null && user != null && strPassword.equals(user.getPassword()))
        return user;

        else
        return null;
    }


    String checkID(User user ){
        if(StringUtil.isBlank(user.getUid()))return "Id를 넣어주세요";
        if(StringUtil.isBlank(user.getPassword()))return "pw를 넣어주세요";

        return null;
    }


    String checkPost(Post post){

        if(StringUtil.isBlank(post.getTitle())) return "제목을 넣어주세요";
        if(StringUtil.isBlank(post.getBody())) return "내용을 넣어주세요";

        return null;
    }

    public String postingnow(Post post){
        String rat = checkPost(post);

        if (rat == null) {
            post.setCreateAt(new Date());
            userMapper.insertpost(post);
        }
        return rat;
    }

    public BoardList boardList(Page criteria){
        BoardList list = new BoardList();
        list.setList(userMapper.findAll(criteria));
        list.setTotal(userMapper.countall());
        return list;
    }

    // public List<Comment> CommentList(Comment comment){
    //     return userMapper.findAllComment(comment);
    // }

    // public List<Post> boardSearchList(String searchKeyword, Criteria criteria){
    //     return userMapper.findByTitleContaining(criteria,searchKeyword);
    // }


    public Post boardViewing(int bid){
        return userMapper.findPostByBid(bid);
    }

    public List<Comment> commentviewing(int bid){ //bid값을 인자로 받아서 넘겨야함
        return userMapper.findCommentByBid(bid);
    }

    public void boardDelete(int bid){

        userMapper.deleteByBid(bid);
    }

    // public void commentDelete(int aid){

    //     userMapper.deleteByAId(aid);
    // }




    public void boardUpdate(Post post){
        Post old = userMapper.findPostByBid(post.getBid());
        old.setBody(post.getBody());
        old.setTitle(post.getTitle());
        old.setUpdateDt(new Date());
        userMapper.updateByID(old);
    }

    String checkComment(Comment comment){

        if(StringUtil.isBlank(comment.getContents()))
        return "댓글내용을 넣어주세요";

        return null;
    }

    public String commenting(Comment comment){
        String cmt = checkComment(comment);

        if (cmt == null) {
            comment.setCreateAt(new Date());
            userMapper.insertcomment(comment);
        }

        return cmt;
    }
}
