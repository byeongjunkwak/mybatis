package com.innilabs.mybatis.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.innilabs.mybatis.dto.AuthInfo;
import com.innilabs.mybatis.entity.User;

public class CookieUtil {

    public static Cookie buildAuthCookie(User user) {
        String v = user.getUid() + ":" + user.getName(); 
        Cookie c = new Cookie("auth", v);
        c.setMaxAge(60*10);
        return c;
    }

    public static AuthInfo getAuthInfo(HttpServletRequest request) {
        Cookie[] cs = request.getCookies(); //HttpServletRequest의 객체 request에서 getcookies()메소드가져오지
        String v = null; //String타입 v하나 만들어줌 

        if (cs != null) { //쿠키가 없는게 아니면 
            for (int i = 0; i < cs.length; i++) {
                Cookie c = cs[i]; //반복문 돌려서 Cookie타입 객체 c에 값넣어줌 계속 추가 
                if (c.getName().equals("auth")) { //객체 c에서 가져온 이름이 auth의 value값이랑 같다면 
                    v = c.getValue(); //만들어둔 v에다가 값 저장 
                }
            }
        }


        if (v != null) { //당연히 null이 아니지 않나 
            AuthInfo auth = new AuthInfo();
            String[] tokens = v.split(":"); //이렇게 스플릿해서
            auth.setUid(tokens[0]); //토큰배열에 id name값 차례로 저장해줌
            auth.setName(tokens[1]);
            return auth;
        }

        return null;
    }
    
    public static void expireCookie(HttpServletResponse response,String cookieName){
        Cookie cookie = new Cookie(cookieName, null);
        cookie.setMaxAge(0);
        response.addCookie(cookie);
    }
}
