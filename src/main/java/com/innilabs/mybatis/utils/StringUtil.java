package com.innilabs.mybatis.utils;

public class StringUtil {
    public static boolean isBlank(String v) { //isBlank란 함수 생성 v를 인자로 받는 
        if (v == null) return true; // v가 null값이면 트루 리턴
        if (v.isBlank()) return true; // v.isBlank?이게 어떻게 되는거지
        return false; //아닐때는 false == blank가 아니라는거지 
    }
}
