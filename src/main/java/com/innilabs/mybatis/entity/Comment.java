package com.innilabs.mybatis.entity;

import java.util.Date;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Data

public class Comment {

    int aid;
    int bid;
    String uid;
    String contents;
    Date createAt;
}
