package com.innilabs.mybatis.entity;

import java.util.Date;
import lombok.Data;

@Data
public class User {
    String uid;
    String name;
    String password;
    String email;
    Date createAt;
}
