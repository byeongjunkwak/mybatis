package com.innilabs.mybatis.entity;

import java.util.Date;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Data
public class Post {

    int bid;
    String uid;
    String name;
    String title;
    String body;
    Date createAt;
    Date updateDt;
    String filename;
    String filepath;
}
