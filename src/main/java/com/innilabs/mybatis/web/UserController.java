package com.innilabs.mybatis.web;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.innilabs.mybatis.dto.AuthInfo;
import com.innilabs.mybatis.entity.User;
import com.innilabs.mybatis.service.UserService;
import com.innilabs.mybatis.utils.CookieUtil;

import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor
public class UserController {
    final UserService userService;

    @GetMapping("/")
    public String home(HttpServletRequest request, Model model){
        AuthInfo auth = CookieUtil.getAuthInfo(request);
        if (auth != null) {
            model.addAttribute("uid", auth.getUid()); //model attribute로 html홈 파일에서 아이디와 유저이름 보여줌
            model.addAttribute("name", auth.getName()); //쿠키에 저장된 id와 name 뽑아야할때 쓰는 메서드
        }
        else {
            model.addAttribute("uid", null); //model attribute로 html홈 파일에서 아이디와 유저이름 보여줌
            model.addAttribute("name", null); //쿠키에 저장된 id와 name 뽑아야할때 쓰는 메서드
        }

        return "home"; // home html 띄워줌 누르는거없는이상 그냥 계속 보여준다는뜻
    }

    @GetMapping("/user") //테스트용
    public String user(String uid, Model model) {
        if (uid == null) {
            model.addAttribute("errormsg", "bad parameter");
            return "err";
        }
        User user = userService.getUserInfo(uid);
        if (user == null) {
            model.addAttribute("errormsg", "not found");
            return "err";
        }
        model.addAttribute("author", user);
        return "test";
    }

    @GetMapping("/signon") //페이지 불러오고
    public String signon(Model model) {

        model.addAttribute("user", new User());

        model.addAttribute("error_message", "");
        return "signon"; //signon html열어줌
    }


    @PostMapping("/signon") //주소는 여전히 같음
    public String signonprocess(User user, Model model) {
        String  ret = userService.registUser(user);
        if (ret == null) {
            //오류메세지가 안떴으니까 출력해주면되지 이제
            //넘길 데이터 내용 집어넣어야함
            model.addAttribute("name", user.getName() );
            return "signon_result";

            //signon_result페이지에 있는 name 이라는 변수에 user.getName()의값을 출력해준다
            //ex)user.getName()님 반갑습니다.
        }
        //아니면 다시 화면을 보여줘야하는데

        model.addAttribute("user", user);
        //user에 포함되있는 객체들은 다 냅두고 빈것만 비게해서 출력

        model.addAttribute("error_message", ret);
        //어떤 오류메세지 출력할껀데? 해서 조건에 맞는 메시지 출력

        return "signon";  //다시 화면 보여주기
    }

    @GetMapping("/login")
    public String loggingin(Model model,String ret) { //로그인안되있어서 넘어왔을때 쓰는 ret
        model.addAttribute("user", new User());

        if(ret == null){
            ret = "/";
        }
        model.addAttribute("ret", ret);

        model.addAttribute("error_message", "");

        return "login";
    }

    @PostMapping("/login")
    public String loginprocess(@RequestParam(value = "uid", defaultValue = "-" )String strLogInId,@RequestParam(value = "password", defaultValue = "-" )String strPassword,Model model, HttpServletResponse response, String ret){

        User user = userService.MemberLogin(strLogInId,strPassword);
        //user객체를 만들고 userService라는 객체리모컨으로 사용하는 MemberLogin 함수를 끌어와서 strid와 strpw를 파라메터로 넣음
        if (user == null) {
            model.addAttribute("msg", "아이디 또는 비밀번호가 일치하지않습니다.");
            return "loginresult";
        }
        if(ret == null){
            ret = "/";
        }
        Cookie c = CookieUtil.buildAuthCookie(user);
        response.addCookie(c);
        //쿠키값 추가한 상태로 원래화면 복귀하면 복귀하면서 원래화면 맨위에있는 메소드 실행되면서 쿠키값이 들어가게됨
        return "redirect:"+ ret;
    }

    @GetMapping("/logout")
    public String logout(HttpServletResponse response){
        CookieUtil.expireCookie(response,"auth");
        return "redirect:/";
    }
}
