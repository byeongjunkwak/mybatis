package com.innilabs.mybatis.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.innilabs.mybatis.dto.BoardList;
import com.innilabs.mybatis.dto.ListRes;
import com.innilabs.mybatis.dto.Page;
import com.innilabs.mybatis.dto.RestRes;
import com.innilabs.mybatis.service.UserService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/mobile")
@RequiredArgsConstructor
public class PostingRestController {
 
    final UserService userService;

    // curl localhost:8081/mobile/boardlist?pagecnt=1&pagesize=10
    @GetMapping("boardlist")
    RestRes<ListRes> getList(@RequestBody Page page) {
        int nowPage = page.getPagecnt() + 1; //지금위치

        int startPage = Math.max(nowPage-4,1);
        //지금위치에서 젤 왼쪽 / 1보다 작게나오면 1이더크기때문에 1반환
    
        int endPage = Math.min(nowPage+5,10);
        //지금위치에서 젤 오른쪽
        //갯수를 받고 갯수 == pagesize 때마다 목록 하나 추가
    
        BoardList list = userService.boardList(page);
    
        ListRes listRes = new ListRes();
        listRes.setList(list);
        listRes.setNowPage(nowPage);
        listRes.setStartPage(startPage);
        listRes.setEndPage(endPage);

        RestRes<ListRes> res = new RestRes<>();
        res.setContents(listRes);
        return res;    
    }
}
