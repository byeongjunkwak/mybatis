package com.innilabs.mybatis.web;

import org.springframework.web.bind.annotation.RestController;

import com.innilabs.mybatis.dto.LoginReq;
import com.innilabs.mybatis.dto.RestRes;
import com.innilabs.mybatis.entity.User;
import com.innilabs.mybatis.service.UserService;

import lombok.RequiredArgsConstructor;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;


@RestController
@RequestMapping("/mobile")
@RequiredArgsConstructor
public class UserRestController {
    final UserService userService;

    @PostMapping(value="/login")
    public RestRes<User> postMethodName(@RequestBody LoginReq login) {
        User user = userService.MemberLogin(login.getUsername(),login.getPassword());

        if (user == null) {
            RestRes<User> res = new RestRes<>(200, "Invalid Auth Info");
            return res;
        }

        RestRes<User> res = new RestRes<>();
        res.setContents(user);
        return res;
    }    
}
