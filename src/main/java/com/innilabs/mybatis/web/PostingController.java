package com.innilabs.mybatis.web;

import java.util.Date;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.innilabs.mybatis.dto.AuthInfo;
import com.innilabs.mybatis.dto.BoardList;
import com.innilabs.mybatis.dto.Page;
import com.innilabs.mybatis.entity.Comment;
import com.innilabs.mybatis.entity.Post;
import com.innilabs.mybatis.service.UserService;
import com.innilabs.mybatis.utils.CookieUtil;

import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor
public class PostingController {
  final UserService userService;

  @GetMapping("/boardWrite")   ///boardwrite 호출받고 넘어옴
  public String boardWriteForm(HttpServletRequest request, Model model){
      model.addAttribute("post", new Post()); //html문서 post자리에 실제 post값넣어줌

      model.addAttribute("error_message", ""); //어차피 입력하기전이어서 에러뜰일 없어서 ""출력

      AuthInfo auth = CookieUtil.getAuthInfo(request);

      if (auth == null) {  //auth에 아무것도없으면
          return "redirect:/login?ret=/boardWrite";
          //로그인 페이지로 넘어갔다가 다시 게시판으로 넘어옴
      }
      return "boardWrite";  //boardWrite페이지를 돌려줌
    }

  @PostMapping("/boardWrite")  //boardWrite페이지에서 post방식으로 bordWrite한테 넘겨주니까 다시옴
  public String boardWriteresult(HttpServletRequest request, Model model,Post post){

    AuthInfo auth = CookieUtil.getAuthInfo(request);
    if (auth == null) {
      return "redirect:/";
    }
    post.setUid(auth.getUid());
    //post리모컨으로 Post에 잇는 uid를 위에서 구한 uid로 바꿈 그래야 밑에서 post에 있는 모든값들을 넘겨줄때 uid도 넘겨져서 들어감

    String  rat = userService.postingnow(post);

    if (rat == null){
      model.addAttribute("id",post.getUid());
      return "boardresult"; //boardresult html을 호출
    }
    model.addAttribute("post", post);
    model.addAttribute("errormessage", rat);
    return "boardWrite"; //다시 boardWrite html을 돌려줌으로써  처음 들어왔던 때랑 똑같은 행위 반복
  }

  @GetMapping("/boardlist")
  public String boardLList(Model model,Page page, String searchKeyword){

    // if(searchKeyword == null){
    //   List<Post> list = userService.boardList(criteria);
    // }else{
    //   List<Post> list = userService.boardSearchList(searchKeyword, criteria);
    // }

  //   model.addAttribute("endPage", endPage);
  //   // model.addAttribute("total", list.getTotal());

  //   return "boardlist";
  // }

	int nowPage = page.getPagecnt() + 1; //지금위치

	int startPage = Math.max(nowPage-4,1);
	//지금위치에서 젤 왼쪽 / 1보다 작게나오면 1이더크기때문에 1반환

	int endPage = Math.min(nowPage+5,10);
	//지금위치에서 젤 오른쪽
	//갯수를 받고 갯수 == pagesize 때마다 목록 하나 추가

	BoardList list = userService.boardList(page);

	model.addAttribute("board", list);
	//모든 리스트들 반환 그걸 list값에 담아서 넘김
	model.addAttribute("nowPage", nowPage);
	model.addAttribute("startPage", startPage);
	model.addAttribute("endPage", endPage);

	return "boardlist";
}
  @GetMapping("/boardview") //url호출받고 들어왔음
  public String visibleboard(HttpServletRequest request, Model model, int bid){

			model.addAttribute("comment", new Comment());

      model.addAttribute("errormessage", "");

    AuthInfo auth = CookieUtil.getAuthInfo(request);

    if (auth == null) {
        return "redirect:/login?ret=/boardview?bid="+ bid;
    }

    model.addAttribute("nowuid", auth.getUid());
    model.addAttribute("delete", "삭제");

    // Service에서 합쳐야 함..
    Post post = userService.boardViewing(bid);
    List<Comment> list = userService.commentviewing(bid); //리스트에 모든값담아서 출력
    model.addAttribute("list", list); //던져줌

    // if(uid.equals(comment.getUid())){

    // }
    model.addAttribute("post", post);

    if (auth.getUid().equals(post.getUid())){ //쓴게시물의 작성자아이디와 지금 쿠키에서 가져온 아이디 정보가 같다면
      return "boardview"; //boardview란 html던져줌
    }
    return "boardviewnotmine"; //boardviewnotmine이란 html 던져줌
    // return "boardview";
  }

   @PostMapping("/boardview") //위에 두개가 뭔지 상관안하고 일단 둘다 post방식으로 boardview라는 주소에 던져줌 그럼 위의 get이아니라 post방식으로 여기로 들어옴 이제 댓글을 구현해야함. 작성버튼 클릭하면 여기로 넘어옴 그리고 다시 원래주소로 보내줘야함?
    public String boardWithComment(Model model,HttpServletRequest request,Comment comment){

    AuthInfo auth = CookieUtil.getAuthInfo(request);
    if (auth == null) {
      return "redirect:/";
    }

    comment.setUid(auth.getUid());
    String cmt = userService.commenting(comment);

    if (cmt == null){
      model.addAttribute("comment", comment);
      return "redirect:/boardview?bid="+ comment.getBid();
    }
    model.addAttribute("comment", comment);
    model.addAttribute("errormessage", cmt);
    // List<Comment> list = userService.commentviewing(bid); 댓글목록가져와 보여주기이걸 위에서 받아야하지?
    return "redirect:/boardview?bid="+ comment.getBid();
  }

  // @GetMapping("/commentDelete")
  //   public String commentdelete(int aid){
  //     userService.commentDelete(aid);
  //     return "redirect:/boardlist";
  // }

  @GetMapping("/boardDelete")
  public String boarddelete(int bid){
    userService.boardDelete(bid);
    return "redirect:/boardlist";
  }

  @GetMapping("/boardEdit/{bid}")
  public String boardEditing(@PathVariable("bid")int bid,Model model){
    //url뒷부분을 자동으로 컷팅해줘서 뒤에 int타입의 변수로 들어와줌
        model.addAttribute("post",userService.boardViewing(bid));
        return "boardEdit";
    }

  @PostMapping("/boardUpdate/{bid}")
  public String boardupdate(@PathVariable("bid") int bid, Post post){

    post.setBid(bid);
    userService.boardUpdate(post);

    return "redirect:/boardlist";
  }

}


